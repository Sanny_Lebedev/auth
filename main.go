//go:generate goagen bootstrap -d bitbucket.org/auth/design

package main

import (
	"fmt"
	"os"

	"bitbucket.org/Sanny_Lebedev/auth/app"
	"bitbucket.org/Sanny_Lebedev/auth/utils/migration"
	//logger "bitbucket.org/auth/logger"
	"bitbucket.org/Sanny_Lebedev/auth/logger"
	"github.com/crgimenes/goconfig"
	_ "github.com/crgimenes/goconfig/json"
	"github.com/goadesign/goa"
	"github.com/goadesign/goa/middleware"
	// "github.com/goadesign/oauth2"
	"github.com/jackc/pgx"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

type Env struct {
	myConf *configMain
}

var (
	// ErrUnauthorized is the error returned for unauthorized requests.
	ErrUnauthorized = goa.NewErrorClass("unauthorized", 401)
	config          *configMain
)

func initConfig() (*configMain, error) {
	myconfig := configMain{}
	goconfig.File = "config.json"
	err := goconfig.Parse(&myconfig)
	if err != nil {
		fmt.Println(err)
		log.Error().Err(err)
		return nil, err
	}
	// fmt.Println(myconfig)
	return &myconfig, nil
}

func pgConnect(cfg *configMain, log logger.Logger) (*pgx.ConnPool, error) {
	pgConfig := new(pgx.ConnConfig)
	pgConfig.TLSConfig = nil
	connPoolConfig := pgx.ConnPoolConfig{
		ConnConfig: pgx.ConnConfig{
			Host:     cfg.Postgres.Host,
			User:     cfg.Postgres.Username,
			Password: cfg.Postgres.Password,
			Database: cfg.Postgres.Database,
			LogLevel: pgx.LogLevelWarn,
			Logger:   log,
		},
		MaxConnections: 15,
	}
	return pgx.NewConnPool(connPoolConfig)
}
func main() {
	// Create service
	service := goa.New("auth")

	env := Env{}
	env.myConf, _ = initConfig()
	fmt.Println(env.myConf.DBMigration)

	file, err := os.OpenFile("debug.log", os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {
		panic(fmt.Sprintf("can't write log file: %v", err))
	}
	defer file.Close()

	log := logger.Logger{
		Logger: zerolog.New(file).With().Timestamp().Str("role", "sad").Logger(),
		// Logger: zerologger,
	}
	pg, err := pgConnect(env.myConf, log)
	if err != nil {
		panic(fmt.Sprintf("no DB connection: ", err))
	}

	// Check current migration status on DB
	migration.MakeMigration(pg, log, env.myConf.DBMigration.Current)

	// Mount middleware
	service.Use(middleware.RequestID())
	service.Use(middleware.LogRequest(true))
	service.Use(middleware.ErrorHandler(service, true))
	service.Use(middleware.Recover())
	service.WithLogger(log.GoaLogger())

	// Mount security middlewares
	//jwtMiddleware, err := NewAuthMiddleware()
	jwtMiddleware, err := NewAuthJWTMiddleware()
	exitOnFailure(err)
	// app.UseBasicAuthMiddleware(service, NewBasicAuthMiddleware())
	app.UseAPIKeyMiddleware(service, NewAPIKeyMiddleware())
	app.UseJWTMiddleware(service, jwtMiddleware)

	// Mount "api_key" controller
	c := NewAPIKeyController(service)
	app.MountAPIKeyController(service, c)
	// Mount "basic" controller
	//c2 := NewBasicController(service)
	//app.MountBasicController(service, c2)

	// Mount "Auth" controller
	c3, err := NewAuthController(service, pg)
	exitOnFailure(err)
	app.MountAuthController(service, c3)

	// Start service
	if err := service.ListenAndServe(":1300"); err != nil {
		service.LogError("startup", "err", err)
	}

}

// exitOnFailure prints a fatal error message and exits the process with status 1.
func exitOnFailure(err error) {
	if err == nil {
		return
	}
	fmt.Fprintf(os.Stderr, "[CRIT] %s", err.Error())
	os.Exit(1)
}
