package design // The convention consists of naming the design
// package "design"
import (
	. "github.com/goadesign/goa/design" // Use . imports to enable the DSL
	. "github.com/goadesign/goa/design/apidsl"
)

var Token = MediaType("application/vnd.token+json", func() {
	Description("A token")
	Attributes(func() {
		Attribute("token", String, "A JWT token")
	})

	View("default", func() {
		Attribute("token")
	})
})

// JWT defines a security scheme using JWT.  The scheme uses the "Authorization" header to lookup
// the token.  It also defines then scope "api".
var JWT = JWTSecurity("jwt", func() {
	Header("Authorization")
	Scope("api:access", "API access") // Define "api:access" scope
})

// Resource jwt uses the JWTSecurity security scheme.
var _ = Resource("jwt", func() {
	Description("This resource uses JWT to secure its endpoints")
	DefaultMedia(SuccessMedia)

	Security(JWT, func() { // Use JWT to auth requests to this endpoint
		Scope("api:access") // Enforce presence of "api" scope in JWT claims.
	})

})

var _ = API("auth", func() { // API defines the microservice endpoint and
	Title("Universal auth system")      // other global properties. There should be one
	Description("The main auth module") // and exactly one API definition appearing in
	Scheme("http")                      // the design.
	Host("localhost:1300")
	Consumes("application/json")
	Produces("application/json")

	// OAuth2 requires form encoding
	Consumes("application/x-www-form-urlencoded", func() {
		Package("github.com/goadesign/goa/encoding/form")
	})
	Produces("application/x-www-form-urlencoded", func() {
		Package("github.com/goadesign/goa/encoding/form")
	})
})

var _ = Resource("auth", func() {
	BasePath("/auth")
	Origin("localhost:1300", func() {
		Methods("POST", "OPTIONS")
		Headers("Authorization", "Content-Type")
		Methods("GET", "POST", "PATCH", "DELETE", "PUT", "OPTION")
	})
	Action("signin", func() {
		Description("Sign in")
		Routing(POST("/signin"))
		Payload(func() {
			Member("email", String)
			Member("password", String)
			Required("email", "password")
		})
		Response(OK, func() {
			Media(Tokens)
		})
		Response(BadRequest, ErrorMedia)
		Response(Unauthorized)
		Response(InternalServerError)
	})
	Action("recover", func() {
		Description("Generate password recovery key")
		Routing(POST("/recover"))
		Payload(func() {
			Member("email", String)
			Required("email")
		})
		Response(OK)
		Response(NotFound)
		Response(InternalServerError)
	})
	Action("change_password", func() {
		Description("Change password")
		Routing(POST("/change_password"))
		Payload(func() {
			Member("key", String)
			Member("password", String)
			Required("key", "password")
		})
		Response(OK)
		Response(NotFound)
		Response(InternalServerError)
	})
	Action("validate", func() {
		Description("Validate JWT")
		Security(JWT)
		Routing(POST("/validate"))
		Response(OK)
		Response(Unauthorized)
		Response(InternalServerError)
	})
	Action("refresh", func() {
		Description("Refresh tokens with refreshToken")
		Security(JWT)
		Routing(GET("/refresh"))
		Response(OK, func() {
			Media(Tokens)
		})
		Response(Unauthorized)
		Response(InternalServerError)
	})

	Action("register", func() {
		NoSecurity()
		Routing(
			POST("/register"),
		)
		Description("Create a new user")
		Payload(RegisterPayload)
		Response(OK, func() {
			Media(Tokens)
		})
		Response(InternalServerError)
		Response(BadRequest, ErrorMedia)

	})

})

var Tokens = MediaType("application/vnd.myserv.auth.token+json", func() {
	ContentType("application/json")
	Attributes(func() {
		Attribute("accessToken", String, "Access JWT")
		Attribute("refreshToken", String, "Refresh JWT")
		Required("accessToken", "refreshToken")
	})
	View("default", func() {
		Attribute("accessToken")
		Attribute("refreshToken")
	})
})

var ApiError = MediaType("application/vnd.myserv.token+json", func() {
	ContentType("application/json")
	Attributes(func() {
		Attribute("APP", String, "APP name")
		Attribute("Module", String, "Module name")
		Attribute("TYPE", String, "TYPE of ERROR")
		Attribute("CODE", String, "CODE of ERROR")
		Attribute("MSG", String, "MESSAGE")

		Required("APP", "Module", "TYPE", "CODE", "MSG")
	})
	View("default", func() {
		Attribute("APP")
		Attribute("Module")
		Attribute("TYPE")
		Attribute("CODE")
		Attribute("MSG")
	})
})

var Key = MediaType("application/vnd.myserv.auth.key+json", func() {
	ContentType("application/json")
	Attributes(func() {
		Attribute("key", String, "String key")
		Required("key")
	})
	View("default", func() {
		Attribute("key")
	})
})

var SuccessMedia = MediaType("application/vnd.goa.examples.security.success", func() {
	Description("The common media type to all request responses for this example")
	TypeName("Success")
	Attributes(func() {
		Attribute("ok", Boolean, "Always true")
		Required("ok")
	})
	View("default", func() {
		Attribute("ok")
	})
})

// BottleMedia defines the media type used to render bottles.
var UserMedia = MediaType("application/vnd.goa.auth.user+json", func() {
	Description("User profile")
	Attributes(func() { // Attributes define the media type shape.
		Attribute("id", Integer, "Unique user ID")
		Attribute("href", String, "API href for making requests on the user profile")
		Attribute("name", String, "UserName")
		Required("id", "href", "name")
	})
	View("default", func() { // View defines a rendering of the media type.
		Attribute("id")   // Media types may have multiple views and must
		Attribute("href") // have a "default" view.
		Attribute("name")
	})
})

var User = MediaType("application/vnd.user+json", func() {
	Description("A user")
	Attributes(func() {
		Attribute("id", Integer, "ID of account", func() {
			Example(1)
		})
		Attribute("email", String, "Email of the user", func() {
			Format("email")
			Example("bob@gmail.com")
		})
		Attribute("first_name", String, "First name of the user", func() {
			Example("John")
		})
		Attribute("last_name", String, "Last name of the user", func() {
			Example("Snow")
		})
		Attribute("main_role", String, "Role of the user", func() {
			Example("Reader")
		})
		Attribute("password", String, "Password of user", func() {
			Example("password")
		})
		Attribute("salt", String, "Salt of the user", func() {
			Example("salt")
		})
	})

	View("default", func() {
		Attribute("id")
		Attribute("email")
		Attribute("first_name")
		Attribute("last_name")
		Attribute("main_role")
		Attribute("password")
		Attribute("salt")
	})
})

var RegisterPayload = Type("RegisterPayload", func() {
	Attribute("email", String, func() {
		MinLength(6)
		MaxLength(150)
		Format("email")
		Example("jamesbond@gmail.com")
	})

	Attribute("first_name", String, func() {
		MinLength(1)
		MaxLength(200)
		Example("John")
	})

	Attribute("last_name", String, func() {
		MinLength(1)
		MaxLength(200)
		Example("Doe")
	})

	Attribute("password", String, func() {
		MinLength(5)
		MaxLength(100)
		Example("abcd1234")
	})

	Required("email", "password", "first_name", "last_name")
})
