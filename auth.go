package main

import (
	"context"
	"crypto/rsa"
	"database/sql"
	"fmt"
	"io/ioutil"
	"net/http"
	"path/filepath"
	"time"

	"bitbucket.org/Sanny_Lebedev/auth/app"
	"bitbucket.org/Sanny_Lebedev/auth/repositories"
	"bitbucket.org/Sanny_Lebedev/auth/utils/users"
	jwtgo "github.com/dgrijalva/jwt-go"
	"github.com/goadesign/goa"
	"github.com/goadesign/goa/middleware/security/jwt"
	"github.com/jackc/pgx"
	uuid "github.com/satori/go.uuid"
)

// TokenUserInfo implements the token resource
type TokenUserInfo struct {
	userID   string
	userName string
	userType string
}

//Token service structure
type TokenStruct struct {
	UserInf   TokenUserInfo
	TokenType string
	ExpTime   float64
}

// AuthController implements the auth resource.
type AuthController struct {
	*goa.Controller
	privateKey *rsa.PrivateKey
	DB         *pgx.ConnPool
}

// NewAuthController creates a auth controller.
func NewAuthController(service *goa.Service, pg *pgx.ConnPool) (*AuthController, error) {

	b, err := ioutil.ReadFile("./keys/rs256-4096-private.rsa")
	if err != nil {
		return nil, err
	}
	privKey, err := jwtgo.ParseRSAPrivateKeyFromPEM(b)
	if err != nil {
		return nil, fmt.Errorf("jwt: failed to load private key: %s", err) // bug
	}
	return &AuthController{
		Controller: service.NewController("AuthController"),
		privateKey: privKey,
		DB:         pg,
	}, nil

	//return &AuthController{Controller: service.NewController("AuthController")}
}

// ChangePassword runs the change_password action.
func (c *AuthController) ChangePassword(ctx *app.ChangePasswordAuthContext) error {
	// AuthController_ChangePassword: start_implement

	// Put your logic here

	return nil
	// AuthController_ChangePassword: end_implement
}

// Recover runs the recover action.
func (c *AuthController) Recover(ctx *app.RecoverAuthContext) error {
	// AuthController_Recover: start_implement

	// Put your logic here

	return nil
	// AuthController_Recover: end_implement
}

// Register runs the register action.
func (c *AuthController) Register(ctx *app.RegisterAuthContext) error {
	// AuthController_Register: start_implement

	// Put your logic here
	payload := ctx.Payload

	//TESTING

	//var mycustomer users.Sysuser
	newuser := new(users.User)
	newuser.DB = c.DB

	//mycustomer = new(users.(*User))
	//mycustomer.DB = c.DB

	newuser.SetUser(payload.FirstName, payload.LastName, payload.Email)
	res1, err := newuser.CheckEmailExists(payload.Email)
	newuser.Customer.Save(c.DB)

	//res1, err := repositories.CheckEmailExists(c.DB, payload.Email)

	if err != nil {
		fmt.Errorf("failed to get: %s", err) // internal error
		if err == sql.ErrNoRows {
			c.Service.LogError("Check emails for dbl", "err", err)
		}
		c.Service.LogError("Users registration", "err", err)
		return ctx.BadRequest(goa.ErrBadRequest("User alredy exists"))

	}

	if res1 == true {
		return ctx.BadRequest(goa.ErrBadRequest("User alredy exists"))
	}

	uid, err_out := repositories.AddUserToDatabase(c.DB, payload.FirstName, payload.LastName, payload.Email, payload.Password)
	if err_out != nil {
		fmt.Errorf("failed to get: %s", err) // internal error
		c.Service.LogError("Users registration", "err", err)
		return ctx.InternalServerError()
	}

	currentTokenInfo := TokenUserInfo{uid, payload.FirstName, "reader"}

	ts := TokenStruct{currentTokenInfo, "AuthToken", float64(time.Now().Add(time.Hour * 3).Unix())}
	AccessTokenString, _, err := c.calcToken(&ts)

	ts = TokenStruct{currentTokenInfo, "RefreshToken", float64(time.Now().Add(time.Hour * 30).Unix())}
	RefreshTokenString, jti, err := c.calcToken(&ts)

	if err != nil {
		return fmt.Errorf("failed to sign token: %s", err) // internal error
	}
	//Making record for refresh tokens table on DB
	out, err := repositories.RefreshTokenToDatabase(c.DB, uid, jti)
	if err != nil {
		fmt.Errorf("Problems with reftoken table: %s", err) // internal error
		c.Service.LogError("Users registration", "err", err)
	}
	if out != false {
		res := &app.MyservAuthToken{AccessTokenString, RefreshTokenString}
		return ctx.OK(res)
	}
	return ctx.InternalServerError()
}

// Refresh runs the refresh action.
func (c *AuthController) Refresh(ctx *app.RefreshAuthContext) error {
	// AuthController_Refresh: start_implement

	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	newuserID := claims["userID"]

	fmt.Print(newuserID) // internal error

	currentTokenInfo := TokenUserInfo{"12", "TestUserName", "Reader"}

	ts := TokenStruct{currentTokenInfo, "AuthToken", float64(time.Now().Add(time.Hour * 3).Unix())}
	AccessTokenString, _, err := c.calcToken(&ts)

	ts = TokenStruct{currentTokenInfo, "RefreshToken", float64(time.Now().Add(time.Hour * 30).Unix())}
	RefreshTokenString, _, err := c.calcToken(&ts)

	if err != nil {
		return fmt.Errorf("failed to sign token: %s", err) // internal error
	}

	res := &app.MyservAuthToken{AccessTokenString, RefreshTokenString}
	return ctx.OK(res)
	// AuthController_Refresh: end_implement
}

// Signin runs the signin action.
func (c *AuthController) Signin(ctx *app.SigninAuthContext) error {

	payload := ctx.Payload
	// u, err := repositories.CheckEmailExists(c.DB, payload.Email)

	u, err := repositories.CheckUser(c.DB, payload.Email, payload.Password)

	if err != nil {
		fmt.Errorf("failed to get: %s", err) // internal error
		if err == sql.ErrNoRows {
			return ctx.BadRequest(goa.ErrBadRequest("Invalid email or password"))
		}
		c.Service.LogError("Login User", "err", err)
		return ctx.BadRequest(goa.ErrBadRequest("Invalid email or password"))
	}
	if u == nil {
		return ctx.BadRequest(goa.ErrBadRequest("Invalid email or password"))
	}

	// Datas about user
	currentTokenInfo := TokenUserInfo{"12", "TestUserName", "Reader"}

	ts := TokenStruct{currentTokenInfo, "AuthToken", float64(time.Now().Add(time.Hour * 3).Unix())}
	AccessTokenString, _, err := c.calcToken(&ts)

	ts = TokenStruct{currentTokenInfo, "RefreshToken", float64(time.Now().Add(time.Hour * 30).Unix())}
	RefreshTokenString, _, err := c.calcToken(&ts)

	if err != nil {
		return fmt.Errorf("failed to sign token: %s", err) // internal error
	}

	res := &app.MyservAuthToken{AccessTokenString, RefreshTokenString}
	return ctx.OK(res)
	// AuthController_Signin: end_implement
}

// Validate runs the validate action.
func (c *AuthController) Validate(ctx *app.ValidateAuthContext) error {
	// AuthController_Validate: start_implement

	// Put your logic here

	return nil
	// AuthController_Validate: end_implement
}

//Generate new JWT Token
func (c *AuthController) genNewToken(ts *TokenStruct) (string, string, error) {

	token := jwtgo.New(jwtgo.SigningMethodRS512)
	u, err := uuid.NewV4()
	if err != nil {
		return "", "", fmt.Errorf("Error: %s", err) // internal error
	}
	jti := u.String()

	token.Claims = jwtgo.MapClaims{
		"userID":   ts.UserInf.userID,
		"userName": ts.UserInf.userName,
		"userType": ts.UserInf.userType,
		"type":     ts.TokenType,
		"iss":      "Issuer",          // who creates the token and signs it
		"aud":      "Audience",        // to whom the token is intended to be sent
		"exp":      ts.ExpTime,        // time when the token will expire (10 minutes from now)
		"jti":      jti,               // a unique identifier for the token
		"iat":      time.Now().Unix(), // when the token was issued/created (now)
		"nbf":      2,                 // time before which the token is not yet valid (2 minutes ago)
		"sub":      "subject",         // the subject/principal is whom the token is about
		"scopes":   "api:access",      // token scope - not a standard claim
	}

	outtoken, err := token.SignedString(c.privateKey)
	return outtoken, jti, err

}

func (c *AuthController) calcToken(g *TokenStruct) (string, string, error) {
	token, jti, _ := c.genNewToken(g)
	return token, jti, nil
}

// NewJWTMiddleware creates a middleware that checks for the presence of a JWT Authorization header
// and validates its content. A real app would probably use goa's JWT security middleware instead.
//
// Note: the code below assumes the example is compiled against the master branch of goa.
// If compiling against goa v1 the call to jwt.New needs to be:
//
func NewAuthJWTMiddleware() (goa.Middleware, error) {
	keys, err := LoadAuthJWTPublicKeys()
	if err != nil {
		return nil, err
	}
	return jwt.New(jwt.NewSimpleResolver(keys), ForceFail(), app.NewJWTSecurity()), nil
}

// LoadJWTPublicKeys loads PEM encoded RSA public keys used to validata and decrypt the JWT.
func LoadAuthJWTPublicKeys() ([]jwt.Key, error) {
	keyFiles, err := filepath.Glob("./keys/*.pem")
	if err != nil {
		return nil, err
	}
	keys := make([]jwt.Key, len(keyFiles))
	for i, keyFile := range keyFiles {
		pem, err := ioutil.ReadFile(keyFile)
		if err != nil {
			return nil, err
		}
		key, err := jwtgo.ParseRSAPublicKeyFromPEM([]byte(pem))
		if err != nil {
			return nil, fmt.Errorf("failed to load key %s: %s", keyFile, err)
		}
		keys[i] = key
	}
	if len(keys) == 0 {
		return nil, fmt.Errorf("couldn't load public keys for JWT security")
	}

	return keys, nil
}

// ForceFail is a middleware illustrating the use of validation middleware with JWT auth.  It checks
// for the presence of a "fail" query string and fails validation if set to the value "true".
func ForceFail() goa.Middleware {
	errValidationFailed := goa.NewErrorClass("validation_failed", 401)
	forceFail := func(h goa.Handler) goa.Handler {
		return func(ctx context.Context, rw http.ResponseWriter, req *http.Request) error {
			if f, ok := req.URL.Query()["fail"]; ok {
				if f[0] == "true" {
					return errValidationFailed("forcing failure to illustrate Validation middleware")
				}
			}
			return h(ctx, rw, req)
		}
	}
	fm, _ := goa.NewMiddleware(forceFail)
	return fm
}
