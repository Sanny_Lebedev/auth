// Code generated by goagen v1.3.1, DO NOT EDIT.
//
// API "auth": auth Resource Client
//
// Command:
// $ goagen
// --design=bitbucket.org/auth/design
// --out=E:\gopath\src\bitbucket.org\auth
// --version=v1.3.1

package client

import (
	"bytes"
	"context"
	"fmt"
	"net/http"
	"net/url"
)

// ChangePasswordAuthPayload is the auth change_password action payload.
type ChangePasswordAuthPayload struct {
	Key      string `form:"key" json:"key" xml:"key"`
	Password string `form:"password" json:"password" xml:"password"`
}

// ChangePasswordAuthPath computes a request path to the change_password action of auth.
func ChangePasswordAuthPath() string {

	return fmt.Sprintf("/auth/change_password")
}

// Change password
func (c *Client) ChangePasswordAuth(ctx context.Context, path string, payload *ChangePasswordAuthPayload, contentType string) (*http.Response, error) {
	req, err := c.NewChangePasswordAuthRequest(ctx, path, payload, contentType)
	if err != nil {
		return nil, err
	}
	return c.Client.Do(ctx, req)
}

// NewChangePasswordAuthRequest create the request corresponding to the change_password action endpoint of the auth resource.
func (c *Client) NewChangePasswordAuthRequest(ctx context.Context, path string, payload *ChangePasswordAuthPayload, contentType string) (*http.Request, error) {
	var body bytes.Buffer
	if contentType == "" {
		contentType = "*/*" // Use default encoder
	}
	err := c.Encoder.Encode(payload, &body, contentType)
	if err != nil {
		return nil, fmt.Errorf("failed to encode body: %s", err)
	}
	scheme := c.Scheme
	if scheme == "" {
		scheme = "http"
	}
	u := url.URL{Host: c.Host, Scheme: scheme, Path: path}
	req, err := http.NewRequest("POST", u.String(), &body)
	if err != nil {
		return nil, err
	}
	header := req.Header
	if contentType == "*/*" {
		header.Set("Content-Type", "application/json")
	} else {
		header.Set("Content-Type", contentType)
	}
	return req, nil
}

// RecoverAuthPayload is the auth recover action payload.
type RecoverAuthPayload struct {
	Email string `form:"email" json:"email" xml:"email"`
}

// RecoverAuthPath computes a request path to the recover action of auth.
func RecoverAuthPath() string {

	return fmt.Sprintf("/auth/recover")
}

// Generate password recovery key
func (c *Client) RecoverAuth(ctx context.Context, path string, payload *RecoverAuthPayload, contentType string) (*http.Response, error) {
	req, err := c.NewRecoverAuthRequest(ctx, path, payload, contentType)
	if err != nil {
		return nil, err
	}
	return c.Client.Do(ctx, req)
}

// NewRecoverAuthRequest create the request corresponding to the recover action endpoint of the auth resource.
func (c *Client) NewRecoverAuthRequest(ctx context.Context, path string, payload *RecoverAuthPayload, contentType string) (*http.Request, error) {
	var body bytes.Buffer
	if contentType == "" {
		contentType = "*/*" // Use default encoder
	}
	err := c.Encoder.Encode(payload, &body, contentType)
	if err != nil {
		return nil, fmt.Errorf("failed to encode body: %s", err)
	}
	scheme := c.Scheme
	if scheme == "" {
		scheme = "http"
	}
	u := url.URL{Host: c.Host, Scheme: scheme, Path: path}
	req, err := http.NewRequest("POST", u.String(), &body)
	if err != nil {
		return nil, err
	}
	header := req.Header
	if contentType == "*/*" {
		header.Set("Content-Type", "application/json")
	} else {
		header.Set("Content-Type", contentType)
	}
	return req, nil
}

// RefreshAuthPath computes a request path to the refresh action of auth.
func RefreshAuthPath() string {

	return fmt.Sprintf("/auth/refresh")
}

// Refresh tokens with refreshToken
func (c *Client) RefreshAuth(ctx context.Context, path string) (*http.Response, error) {
	req, err := c.NewRefreshAuthRequest(ctx, path)
	if err != nil {
		return nil, err
	}
	return c.Client.Do(ctx, req)
}

// NewRefreshAuthRequest create the request corresponding to the refresh action endpoint of the auth resource.
func (c *Client) NewRefreshAuthRequest(ctx context.Context, path string) (*http.Request, error) {
	scheme := c.Scheme
	if scheme == "" {
		scheme = "http"
	}
	u := url.URL{Host: c.Host, Scheme: scheme, Path: path}
	req, err := http.NewRequest("GET", u.String(), nil)
	if err != nil {
		return nil, err
	}
	if c.JWTSigner != nil {
		if err := c.JWTSigner.Sign(req); err != nil {
			return nil, err
		}
	}
	return req, nil
}

// RegisterAuthPath computes a request path to the register action of auth.
func RegisterAuthPath() string {

	return fmt.Sprintf("/auth/register")
}

// Create a new user
func (c *Client) RegisterAuth(ctx context.Context, path string, payload *RegisterPayload, contentType string) (*http.Response, error) {
	req, err := c.NewRegisterAuthRequest(ctx, path, payload, contentType)
	if err != nil {
		return nil, err
	}
	return c.Client.Do(ctx, req)
}

// NewRegisterAuthRequest create the request corresponding to the register action endpoint of the auth resource.
func (c *Client) NewRegisterAuthRequest(ctx context.Context, path string, payload *RegisterPayload, contentType string) (*http.Request, error) {
	var body bytes.Buffer
	if contentType == "" {
		contentType = "*/*" // Use default encoder
	}
	err := c.Encoder.Encode(payload, &body, contentType)
	if err != nil {
		return nil, fmt.Errorf("failed to encode body: %s", err)
	}
	scheme := c.Scheme
	if scheme == "" {
		scheme = "http"
	}
	u := url.URL{Host: c.Host, Scheme: scheme, Path: path}
	req, err := http.NewRequest("POST", u.String(), &body)
	if err != nil {
		return nil, err
	}
	header := req.Header
	if contentType == "*/*" {
		header.Set("Content-Type", "application/json")
	} else {
		header.Set("Content-Type", contentType)
	}
	return req, nil
}

// SigninAuthPayload is the auth signin action payload.
type SigninAuthPayload struct {
	Email    string `form:"email" json:"email" xml:"email"`
	Password string `form:"password" json:"password" xml:"password"`
}

// SigninAuthPath computes a request path to the signin action of auth.
func SigninAuthPath() string {

	return fmt.Sprintf("/auth/signin")
}

// Sign in
func (c *Client) SigninAuth(ctx context.Context, path string, payload *SigninAuthPayload, contentType string) (*http.Response, error) {
	req, err := c.NewSigninAuthRequest(ctx, path, payload, contentType)
	if err != nil {
		return nil, err
	}
	return c.Client.Do(ctx, req)
}

// NewSigninAuthRequest create the request corresponding to the signin action endpoint of the auth resource.
func (c *Client) NewSigninAuthRequest(ctx context.Context, path string, payload *SigninAuthPayload, contentType string) (*http.Request, error) {
	var body bytes.Buffer
	if contentType == "" {
		contentType = "*/*" // Use default encoder
	}
	err := c.Encoder.Encode(payload, &body, contentType)
	if err != nil {
		return nil, fmt.Errorf("failed to encode body: %s", err)
	}
	scheme := c.Scheme
	if scheme == "" {
		scheme = "http"
	}
	u := url.URL{Host: c.Host, Scheme: scheme, Path: path}
	req, err := http.NewRequest("POST", u.String(), &body)
	if err != nil {
		return nil, err
	}
	header := req.Header
	if contentType == "*/*" {
		header.Set("Content-Type", "application/json")
	} else {
		header.Set("Content-Type", contentType)
	}
	return req, nil
}

// ValidateAuthPath computes a request path to the validate action of auth.
func ValidateAuthPath() string {

	return fmt.Sprintf("/auth/validate")
}

// Validate JWT
func (c *Client) ValidateAuth(ctx context.Context, path string) (*http.Response, error) {
	req, err := c.NewValidateAuthRequest(ctx, path)
	if err != nil {
		return nil, err
	}
	return c.Client.Do(ctx, req)
}

// NewValidateAuthRequest create the request corresponding to the validate action endpoint of the auth resource.
func (c *Client) NewValidateAuthRequest(ctx context.Context, path string) (*http.Request, error) {
	scheme := c.Scheme
	if scheme == "" {
		scheme = "http"
	}
	u := url.URL{Host: c.Host, Scheme: scheme, Path: path}
	req, err := http.NewRequest("POST", u.String(), nil)
	if err != nil {
		return nil, err
	}
	if c.JWTSigner != nil {
		if err := c.JWTSigner.Sign(req); err != nil {
			return nil, err
		}
	}
	return req, nil
}
