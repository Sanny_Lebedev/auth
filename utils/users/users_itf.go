package users

import (
	"fmt"

	"github.com/jackc/pgx"
)

type Sysuser interface {
	New(db *pgx.ConnPool)
	Save(db *pgx.ConnPool)
	SetUser(firstName, lastName, email string)
	CheckEmailExists(email string) (bool, error)
}

type User struct {
	DB *pgx.ConnPool
	Customer
	admin
	seller
}

type UserAbout struct {
	name     string
	lastName string
	ip       string
}

type userStatus struct {
	roleCode   int8
	roleName   string
	statusCode int8
	statusName string
}

type userInfo struct {
	uid string
	UserAbout
	userStatus
	userContacts
	userCoord
	userAdress
}

type userAdress struct {
	postCode string
	country  string
	city     string
	street   string
	building string
	office   string
}

type userCoord struct {
	lat float32
	lon float32
}

type userContacts struct {
	phone string
	email string
}

type Customer struct {
	customerUID string
	userInfo
}

type seller struct {
	sellerUID string
	userInfo
}

type admin struct {
	adminUID string
	userInfo
}

func (p *Customer) Save(db *pgx.ConnPool) {
	query := p.getQuery("saveNew")
	fmt.Println(p.UserAbout)
	fmt.Println(query)
	return
}

func (p admin) Save(db *pgx.ConnPool) {
	return
}

func (p seller) Save(db *pgx.ConnPool) {
	var query []Queries
	p.getQuery("saveNew")
	// query = p.getQuery("saveNew")
	fmt.Println(p.UserAbout)
	fmt.Println(query)
	return
}

func (p *Customer) SetUser(firstName, lastName, email string) {
	p.UserAbout.ip = "0.0.0.0"
	p.UserAbout.name = firstName
	p.UserAbout.lastName = lastName
	p.userContacts.email = email
	return
}

func (u User) CheckEmailExists(email string) (bool, error) {
	//var query []Queries
	str := u.getQuery("CheckEmailExists")
	println(str)
	sqlstr := str[0]["SQL"]
	//const sqlstr = "select exists(select 1 from users where email = $1)"
	var exists bool
	err := u.DB.QueryRow(sqlstr, email).Scan(&exists)
	return exists, err
}
