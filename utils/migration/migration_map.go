package migration

import "github.com/jackc/pgx"

func getMigration(pg *pgx.ConnPool, num int) []Migration {
	var mig = map[int][]Migration{
		0: []Migration{
			Migration{
				Action: NewPgQuery(pg, `CREATE TABLE IF NOT EXISTS users (
										first_name     varchar(255),
										last_name      varchar(255),
										main_role      varchar(50),
										email     	   varchar(50),
										password       varchar(255),
										salt     	   varchar(255),
										id     		   uuid PRIMARY KEY DEFAULT gen_random_uuid(),
										regdata	       timestamp );`,
					nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE TABLE IF NOT EXISTS ConfigTab (
										key     varchar(50),
										val		varchar(50) );`,
					nil),
			},
			Migration{
				Action: NewPgQuery(pg, `INSERT INTO ConfigTab (key, val) VALUES ('currentMigration', '2');`, nil),
			},
			Migration{
				Action: NewPgQuery(pg,
					`CREATE TABLE IF NOT EXISTS reftokens ( uid	uuid, jti	uuid);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg,
					`CREATE INDEX IF NOT EXISTS jti_index ON reftokens (jti);`, nil),
			},

			Migration{
				Action: NewPgQuery(pg,
					`CREATE INDEX IF NOT EXISTS useremail_index ON users (email);`, nil),
			},

			Migration{
				Action: NewPgQuery(pg,
					`CREATE INDEX IF NOT EXISTS confkey_index ON ConfigTab (key);`, nil),
			},
		},
		1: []Migration{
			Migration{
				Action: NewPgQuery(pg, `CREATE TABLE IF NOT EXISTS passwordrecover (
										id     		   uuid PRIMARY KEY DEFAULT gen_random_uuid(),
										regdata	       timestamp,
										UserID		   uuid,
										keyRecover	   uuid,
										active		   bool
										 );`,
					nil),
			},
			Migration{
				Action: NewPgQuery(pg,
					`CREATE INDEX keyRecord_passRecover ON passwordrecover(keyRecover);`, nil),
			},
		}}
	return mig[num]
}
