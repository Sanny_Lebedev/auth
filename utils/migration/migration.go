package migration

import (
	"fmt"
	"strconv"

	"bitbucket.org/Sanny_Lebedev/auth/logger"
	"github.com/jackc/pgx"
	"github.com/rs/zerolog/log"
)

// MakeMigration - function for starting current DB migration
func MakeMigration(db *pgx.ConnPool, log logger.Logger, endMigrationNumber int) {

	curMigrationVersion, _ := checkConfigTab(db, log)

	// zero level migration
	if curMigrationVersion == 0 {
		currentMigration := getMigration(db, 0)
		for _, migration := range currentMigration {
			err := migration.Action.Do()
			if err != nil {
				fmt.Println(err)
				log.Error().Msgf("Error whis migration: ", err)
				log.Error().
					Err(err).
					Str("service", "checkMigration").
					Msgf("Zerolevel migration error: %s", err)
			}
		}

	}

	// if migration level in config file more than level in database
	if curMigrationVersion < endMigrationNumber {
		// if we get zero level migration before
		if curMigrationVersion == 0 {
			curMigrationVersion = 1
		}

		for i := curMigrationVersion; i <= endMigrationNumber; i++ {
			currentMigration := getMigration(db, i)
			for _, migration := range currentMigration {
				err := migration.Action.Do()
				if err != nil {
					fmt.Println(err)
					log.Error().
						Err(err).
						Str("service", "checkMigration").
						Msgf("Process migration: %s", err)
				}
			}
		}

		err := updateMigrationStatusInDb(db, endMigrationNumber)
		if err != nil {
			log.Error().
				Err(err).
				Str("service", "checkMigration").
				Msgf("Process migration: error: %s", err)
		} else {
			log.Info().
				Str("service", "checkMigration").
				Msg("Process migration: success")
		}
	}

}

func updateMigrationStatusInDb(db *pgx.ConnPool, curstatus int) error {
	//Updating current migration status

	status := strconv.Itoa(curstatus)
	const sqlstr = ` UPDATE ConfigTab SET val = $1 WHERE key = 'currentMigration'`
	_, err := db.Exec(sqlstr, status)
	if err != nil {
		fmt.Println(err)
		log.Error().
			Err(err).
			Str("service", "checkMigration").
			Msgf("Updating current migration status error: %s", err)
	}

	return err
}

func checkConfigTab(db *pgx.ConnPool, log logger.Logger) (int, error) {
	const sqlstr = `select val from ConfigTab where key = 'currentMigration' ORDER BY key LIMIT 1`
	var curMigrationVersion string
	err := db.QueryRow(sqlstr).Scan(&curMigrationVersion)
	if err != nil {
		log.Error().
			Err(err).
			Str("service", "checkMigration").
			Msgf("Checking currentMigration: %s", err)

	}
	out, err := strconv.Atoi(curMigrationVersion)

	return out, err
}
