CREATE TABLE IF NOT EXISTS ConfigTab (
		key     string,
		val		string 
	);

CREATE TABLE IF NOT EXISTS reftokens (
		uid     uuid UNIQUE,
		jti		uuid 
	);

CREATE TABLE IF NOT EXISTS users (
		uid             uuid UNIQUE,
		first_name		VARCHAR (250),
        last_name		VARCHAR (250),  
        main_role		VARCHAR (100),
        email		    VARCHAR (50),
        password		VARCHAR (100),
        salt		    VARCHAR (100),
        regdata         TIMESTAMP
	);

INSERT INTO ConfigTab (key, val) VALUES ("currentMigration", "1")