package main

//JWT Token
type Token struct {
	Token string `json:"token"`
}

type mongoDB struct {
	Host string `json:"host" cfg:"host" cfgDefault:"example.com"`
	Port int    `json:"port" cfg:"port" cfgDefault:"999"`
}
type dbmigration struct {
	Current  int    `json:"current" cfg:"current" cfgDefault:"1"`
}

type postgres struct {
	Host     string `json:"host" cfg:"host" cfgDefault:"example.com"`
	Port     int    `json:"port" cfg:"port" cfgDefault:"999"`
	Password string `json:"password" cfg:"password" cfgDefault:"123"`
	Username string `json:"username" cfg:"username" cfgDefault:"username"`
	Database string `json:"database" cfg:"database" cfgDefault:"database"`
}

type systemAPP struct {
	Name string `json:"name" cfg:"name"`
	Port string `json:"port" cfg:"port"`
}

type systemUser struct {
	Name     string `json:"name" cfg:"name"`
	Password string `json:"passwd" cfg:"passwd"`
}

type systemLog struct {
	Level string `json:"level" cfg:"level"`
	Path  string `json:"path" cfg:"path"`
	File  string `json:"file" cfg:"file"`
}

type configMain struct {
	DebugMode bool `json:"debug" cfg:"debug" cfgDefault:"false"`
	Domain    string
	APP       systemAPP  `json:"app" cfg:"app"`
	User      systemUser `json:"user" cfg:"user"`
	Logparam  systemLog  `json:"log" cfg:"log"`
	MongoDB   mongoDB    `json:"mongodb" cfg:"mongodb"`
	Postgres  postgres   `json:"postgres" cfg:"postgres"`
	DBMigration dbmigration `json:"dbmigration" cfg:"dbmigration"`
}
