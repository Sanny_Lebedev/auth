package repositories

import (
	"fmt"

	"github.com/jackc/pgx"
)

//NewDBfromRToken - creating new table for refresh token
func NewDBfromRToken(db *pgx.ConnPool, tablename, uid, jid string) error {
	const sqlstr = `
		CREATE TABLE IF NOT EXISTS $1 (
		uid     uuid UNIQUE,
		jti		uuid 
	);
	`

	res, err := db.Exec(sqlstr, tablename)
	fmt.Println(err)
	fmt.Println(res)
	if err != nil {
		fmt.Errorf("Problems with inserting to DB: %s", err) // internal error
	}

	return err
}

//AddNewRtokenRecord - adding new record to DB
func AddNewRtokenRecord(db *pgx.ConnPool, tablename, uid, jid string) (string, error) {
	const sqlstr = `
	delete from reftokens where uid = $1;
	insert into reftokens (
        uid,
        jti        
    ) values (
        $1,
        $2
    ) 
	`
	var uidd string
	err := db.QueryRow(sqlstr, uid, jid).Scan(&uidd)
	if err != nil {
		fmt.Errorf("Problems with inserting to DB: %s", err) // internal error
	}
	return uidd, err
}

//Checking for table in DB
func CheckDB(db *pgx.ConnPool, tablename string) (bool, error) {

	const sqlstr = `select * from pg_tables where tablename = $1`
	var exists bool
	err := db.QueryRow(sqlstr, tablename).Scan(&exists)

	return exists, err
}
