package repositories

import (
	"fmt"

	"bitbucket.org/Sanny_Lebedev/auth/app"
	"bitbucket.org/Sanny_Lebedev/auth/utils/crypto"
	"github.com/jackc/pgx"
)

// GetUserByEmail gets a user by email
func GetUserByEmail(db *pgx.ConnPool, email string) (*app.User, error) {
	const sqlstr = `
	select 
		first_name,
		last_name,
		main_role,
		email,
        password,
		salt
	from users 
	where email = $1
	`
	var user app.User
	err := db.QueryRow(sqlstr, email).Scan(&user.FirstName, &user.LastName, &user.MainRole, &user.Email, &user.Password, &user.Salt)
	return &user, err
}

// CheckEmailExists just testing if email is on db
func CheckEmailExists(db *pgx.ConnPool, email string) (bool, error) {
	const sqlstr = "select exists(select 1 from users where email = $1)"
	var exists bool
	err := db.QueryRow(sqlstr, email).Scan(&exists)
	return exists, err
}

// Get user from db
func CheckUser(db *pgx.ConnPool, email string, pass string) (*app.User, error) {

	user1, err := GetUserByEmail(db, email)
	if err != nil {
		return nil, err
	}

	if user1.Salt == nil || user1.Password == nil {
		return nil, err
	}
	salt := user1.Salt
	hashedPassword := crypto.HashPassword(pass, *salt)
	if hashedPassword != *user1.Password {
		return nil, err
	}

	return user1, err

}

// AddUserToDatabase creates a new user
func AddUserToDatabase(db *pgx.ConnPool, firstName, lastName, email, password string) (string, error) {

	const sqlstr = `
    insert into users (
        first_name,
        last_name,
        email,
        password,
		salt,
		regdata
    ) values (
        $1,
        $2,
        $3,
        $4,
		$5,
		now()
    ) returning id
    `
	salt := crypto.GenerateSalt()
	hashedPassword := crypto.HashPassword(password, salt)
	var id string
	err := db.QueryRow(sqlstr, firstName, lastName, email, hashedPassword, salt).Scan(&id)
	return id, err
}

// RefrashTokenToDatabase - refreshing token for user on DB
func RefreshTokenToDatabase(db *pgx.ConnPool, uid, jti string) (bool, error) {
	tablename := "reftokens"
	// Check table on DB
	/*
		res, err := CheckDB(db, tablename)
		if err != nil {
			fmt.Errorf("Problems with reftoken table: %s", err) // internal error
		}

		if res == false {
			// making that table
			err := NewDBfromRToken(db, tablename, uid, jti)
			if err != nil {
				fmt.Errorf("Problems with reftoken table: %s", err) // internal error
			}
		}
	*/
	// insert new tokens record to DB
	_, errdb := AddNewRtokenRecord(db, tablename, uid, jti)
	if errdb != nil {
		fmt.Errorf("Problems with inserting to DB: %s", errdb) // internal error
	}

	return true, errdb
}
